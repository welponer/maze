package eu.it.welponer.maze;

import org.apache.commons.io.FilenameUtils;
import org.kohsuke.args4j.*;
import java.io.File;
import java.io.FilenameFilter;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class Main {

    @Argument(required = true, metaVar = "Input file")
    File inFile;

    @Option(name = "-o", metaVar = "output")
    File outFile = new File("");

    @Option(name = "-p", metaVar = "animation period (ms)")
    int animationPeriod = 0;

    private void parseArgs(String[] args) {
        CmdLineParser parser = new CmdLineParser(this);

        try {
            // parse the arguments.
            parser.parseArgument(args);

        } catch( CmdLineException e ) {
            // if there's a problem in the command line,
            // you'll getQTB this exception. this will report
            // an error message.
            System.err.println(e.getMessage());
            System.err.print("Usage: Maze");
            parser.printSingleLineUsage(System.err);
            System.err.println();
            // print the list of available options
            parser.printUsage(System.err);
            System.err.println();
            System.err.print("Note: animation period set to 0 means no animation\n");

            // print option sample. This is useful some time
//            System.err.println("  Example: maze "+parser.printExample(OptionHandlerFilter.ALL));
            System.exit(1);
        }
    }

    private void run() throws Exception {
        Path path = Paths.get(inFile.toURI());
        List<String> data = Files.readAllLines(path);


        Maze maze = new Maze(data, animationPeriod);
        char[][] solution = maze.solve();

        // convert char array to list of strings
        List<String> dataOut = new ArrayList<>();
        for (int i = 0; i < solution.length; i++) {
            StringBuilder line = new StringBuilder();
            for (int j = 0; j < solution[0].length; j++) {
                line.append(solution[i][j] + " ");
            }
            dataOut.add(line.toString());
        }

        // writing result
        try {
            if(outFile.getName() == "")
                outFile = new File(FilenameUtils.removeExtension(inFile.getName()) + "_result.txt");

            Path out = Paths.get(outFile.toURI());
            Files.createFile(out);
            Files.write(out, dataOut);
        }catch(FileAlreadyExistsException foee){
            System.out.println("\nWARNING! the output file already exists");
        }
    }

    public static void main(String[] args) throws Exception {
        Main main = new Main();
        main.parseArgs(args);
        main.run();
    }
}
