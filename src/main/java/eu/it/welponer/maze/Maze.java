package eu.it.welponer.maze;

import lombok.Getter;
import lombok.Setter;
import java.awt.Dimension;
import java.util.List;


public class Maze {

    private char[][] maze;
    private Dimension dimension;
    private int x_bound, y_bound;

    @Getter @Setter private Location start;
    @Getter @Setter private Location end;

    private int ANIMATION_PERIOD;

    /**
     * Constructor
     * - prepare the maze data structure, set end and start locations
     * - set horizontal and vertical boundary coordinate
     * - format the maze
     * @param data maze input in the form of List of rows
     * @param animationPeriod to set the animation speed, if 0 solving animation is not printed
     */
    public Maze(List<String> data, int animationPeriod) {
        ANIMATION_PERIOD = animationPeriod;

        // parse dimension (first row)
        String[] line = data.get(0).split(" ");
        dimension = new Dimension(Integer.parseInt(line[0]), Integer.parseInt(line[1]));
        System.out.println("dimension: " + dimension.width + ", " + dimension.height);

        // find the bounding coordinates
        x_bound = dimension.width - 1;
        y_bound = dimension.height - 1;

        // parse starting location (second row)
        line = data.get(1).split(" ");
        start = new Location(Integer.parseInt(line[0]), Integer.parseInt(line[1]));
        System.out.println("start at (" + start.getX() + ", " + start.getY() + ")");

        // parse end location (third row)
        line = data.get(2).split(" ");
        end = new Location(Integer.parseInt(line[0]), Integer.parseInt(line[1]));
        System.out.println("end at (" + end.getX() + ", " + end.getY() + ")");

        // simple input file check
        if(data.size() != dimension.height + 3)
            System.exit(0);

        // fill the maze input array
        maze = new char[dimension.height][dimension.width];

        for (int i = 0; i < dimension.height; i++) {
            line =  data.get(i+3).split(" ");
            for (int j = 0; j < dimension.width; j++)
                maze[i][j] = line[j].charAt(0);
        }
        print(maze, "\nmaze:");

        // format the maze
        format(maze);
        print(maze, "\nformatted maze:");
    }

    /**
     * Format the maze
     * walls marked by '#', passages marked by ' ', path marked by 'X', start/end marked by 'S'/'E'
     * @param input
     * @return the formatted maze
     */
    public char[][] format(char[][] input) {
        for (int i = 0; i < dimension.height; i++)
            for (int j = 0; j < dimension.width; j++)
                if (input[i][j] == '1') maze[i][j] = '#'; else maze[i][j] = ' ';

//        update(start, 'S');
//        update(end,'E');

        maze[start.getY()][start.getX()] = 'S';
        maze[end.getY()][end.getX()] = 'E';

        return maze;
    }

    /**
     * Clean the maze, removing the visited 'V',
     * restoring the starting location
     */
    private void clean() {
        for (int i = 0; i < dimension.height; i++)
            for (int j = 0; j < dimension.width; j++)
                if(maze[i][j] == 'V')
                    maze[i][j] = ' ';

//        update(start, 'S');
        maze[start.getY()][start.getX()] = 'S';
    }

    /**
     * Solves and prints the maze (if a solution exsists)
     * @return the maze solution
     */
    public char[][] solve() {
        if(solve(start)) {
            clean();
            System.out.println("\nHOORAY!!!");
            print(maze, "\nmaze solution:");
        }else {
            maze[start.getY()][start.getY()] = 'S';
            print(maze, "\nresult:");
            System.out.println("\nThis maze has no solution :(");
        }
        return maze;
    }

    /**
     * recursive solving function
     * @param loc
     * @return true/false if a solution is found
     */
    private boolean solve(Location loc) {
        if(loc.equals(end))
            return true;

        if(valueAt(loc) == '#' || valueAt(loc) == 'X' || valueAt(loc) == 'V')
            return false;

        // mark the position as traversed with 'X'
        update(loc, 'X');

        // TRY SOUTH
        // if we reached the south bound but we can't wrap
        if(loc.getY() == y_bound && valueAt(loc.getX(), 0) == '#') {
            update(loc, 'V');  // mark the position as visited with 'V'
            loc.goNorth();  // get back to previous position
        }
        // recursively (DepthFirst) caring about possible wrapping
        if(solve(loc.south(y_bound))) return true;


        // TRY WEST
        if(loc.getX() == 0 && valueAt(x_bound, loc.getY()) == '#') {
            update(loc, 'V');
            loc.goEast();
        }
        if(solve(loc.west(x_bound))) return true;


        // TRY EAST
        if(loc.getX() == x_bound && valueAt(0, loc.getY()) == '#') {
            update(loc, 'V');
            loc.goWest();
        }
        if(solve(loc.east(x_bound))) return true;


        // TRY NORTH
        if(loc.getY() == 0 && valueAt(loc.getX(), y_bound) == '#') {
            update(loc, 'V');
            loc.goSouth();
        }
        if(solve(loc.north(y_bound))) return true;

        // mark the position as visited with 'V'
        update(loc, 'V');

        return false;
    }

    /**
     * returns the value at location l
     * @param l
     * @return
     */
    private char valueAt(Location l){
        return maze[l.getY()][l.getX()];
    }

    /**
     * returns the value at location with coordinates x, y
     * @param x
     * @param y
     * @return
     */
    private char valueAt(int x, int y){
        return maze[y][x];
    }

    /**
     * update value with char c at location l
     * @param l
     * @param c
     */
    private void update(Location l, char c) {
        if(ANIMATION_PERIOD != 0)
            try {
                Thread.sleep(ANIMATION_PERIOD);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        maze[l.getY()][l.getX()] = c;

        if(ANIMATION_PERIOD != 0)
            print(maze, "\nexploring at ("+ l.getX() + ", " + l.getY() + ")");
    }

    /**
     * print the maze
     * @param array
     * @param title to label the maze
     */
    public void print(char[][] array, String title){
        System.out.println(title);
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[0].length; j++) {
                if(j == array[0].length - 1)
                    System.out.print(array[i][j] + "\n");
                else
                    System.out.print(array[i][j] + " ");
            }
        }
    }
}
