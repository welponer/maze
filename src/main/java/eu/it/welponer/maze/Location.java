package eu.it.welponer.maze;

import lombok.Getter;
import lombok.Setter;

public class Location {

    @Getter @Setter private int x, y;

    public Location(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * checks if two locations are equal
     * @param other
     * @return
     */
    public boolean equals(Location other){
        if (this.x == other.x && this.y == other.y)
            return true;

        return false;
    }

    /**
     * return the coordinates of the pointed position
     */
    public Location north(){return new Location(x, y - 1);}
    public Location south(){return new Location(x, y + 1);}
    public Location west(){return new Location(x - 1, y);}
    public Location east(){return new Location(x + 1, y);}

    /**
     * move to the pointed position and return its coordinates
     */
    public Location goNorth(){return new Location(x, --y);}
    public Location goSouth(){return new Location(x, ++y);}
    public Location goWest(){return new Location(--x, y);}
    public Location goEast(){return new Location(++x, y);}


    /**
     * return the coordinates of the pointed position
     * if wrapping applies return the wrapped coordinates
     */
    public Location north(int y_bound){
        if(y == 0) return new Location(x, y_bound);
        return new Location(x, y - 1);
    }
    public Location south(int y_bound){
        if(y == y_bound) return new Location(x, 0);
        return new Location(x, y + 1);
    }
    public Location west(int x_bound){
        if(x == 0) return new Location(x_bound, y);
        return new Location(x - 1, y);
    }
    public Location east(int x_bound){
        if(x == x_bound) return new Location(0, y);
        return new Location(x + 1, y);
    }

    /**
     * move to the pointed position and return its coordinates
     * if wrapping applies moves to and return the wrapped coordinates
     */
    public Location goNorth(int y_bound){
        if(y == 0){ y = y_bound; return new Location(x, y_bound);}
        return new Location(x, --y);
    }
    public Location goSouth(int y_bound){
        if(y == y_bound){ y = 0; return new Location(x, 0);}
        return new Location(x, ++y);
    }
    public Location goWest(int x_bound){
        if(x == 0){ x = x_bound; return new Location(x_bound, y);}
        return new Location(--x, y);
    }
    public Location goEast(int x_bound){
        if(x == x_bound){ x = 0; return new Location(0, y);}
        return new Location(++x, y);
    }

    public String toString(){
        return "(" + x + ", " + y + ")";
    }

//    public static void main(String[] args){
//        Location l = new Location(0, 0);
//        System.out.println(l.toString());
//
//        l.goEast();
//        System.out.println(l.toString());
//
//        l.east();
//        System.out.println(l.toString());
//
//        l.east();
//        System.out.println(l.toString());
//
//        l.goEast().goEast();
//        System.out.println(l.toString());
//
////        l.goEast();
////        System.out.println(l.toString());
//
//
////        l = l.goWest().goWest().goWest();
////        System.out.println(l.toString());
////
////        System.out.println(l.west().toString());
////        System.out.println(l.toString());
////
////        System.out.println(l.west(10).toString());
////        System.out.println(l.toString());
//    }
}
