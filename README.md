MAZE
===
Will solve any solvable maze. The solution will be written to the console and into an output file. If no solution is found, the output file will contain all the visited locations. Output files will not be overwritten, a warning will be risen.

It is possible to enable the step-by-step maze walk console print out, and to regulate its speed.

Compiled in IntelliJ IDEA community 2019.

Usage: maze Input file [-o output] [-p animation period (ms)]
Note: animation period set to 0 means no step-by-step animation

Example:
```bash
$ java -jar maze-1.0-SNAPSHOT.jar maze.txt -p 33
```

